<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\MapController;
use App\Http\Controllers\BookmarkController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('api')->group(function () {
    //API user
    Route::get('/user/list', [UserController::class, 'getAll']);
    Route::post('/user/store', [UserController::class, 'create']);
    Route::put('/user/update/{id}', [UserController::class, 'update']);
    Route::delete('/user/delete/{id}', [UserController::class, 'delete']);
    Route::get('/user/show/{id}', [UserController::class, 'show']);

    //API bookmark
    Route::get('/bookmark/list', [BookmarkController::class, 'getAll']);
    Route::post('/bookmark/store', [BookmarkController::class, 'create']);
    Route::put('/bookmark/update/{id}', [BookmarkController::class, 'update']);
    Route::delete('/bookmark/delete/{id}', [BookmarkController::class, 'delete']);
    Route::get('/bookmark/show/{id}', [BookmarkController::class, 'show']);
    Route::post('/bookmark/get_by_user_and_map', [BookmarkController::class, 'getByUserAndMap']);

    //API map
    Route::get('/map/list', [MapController::class, 'getAll']);
    Route::post('/map/store', [MapController::class, 'create']);
    Route::put('/map/update/{id}', [MapController::class, 'update']);
    Route::delete('/map/delete/{id}', [MapController::class, 'delete']);
    Route::get('/map/show/{id}', [MapController::class, 'show']);
    Route::post('/map/findByWMS', [MapController::class, 'findByWMS']);
});
