<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users_map', function (Blueprint $table) {
            $table->increments("id");
            $table->string("user_name", 100);
            $table->integer("map_id");
            $table->timestamps();

            $table->foreign('map_id')->references('id')->on('map');
            $table->foreign('user_name')->references('user_name')->on('users');
            $table->unique(["user_name", "map_id"]);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users_map');
    }
};
