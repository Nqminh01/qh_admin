<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service\Implement\UserService;


class UserController
{
    public $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function getAll()
    {
        $users = $this->userService->getAll();
        return response()->json($users);
    }

    public function show(int $id)
    {
        $user = $this->userService->show($id);
        return response()->json($user);
    }

    public function create(Request $request)
    {
        $this->userService->create($request);
        return response()->json("Created");
    }

    public function update(Request $request, int $id)
    {
        $this->userService->update($request, $id);
        return response()->json("Updated");
    }

    public function delete(int $id)
    {
        $this->userService->delete($id);
        return response()->json("Deleted");
    }
}
