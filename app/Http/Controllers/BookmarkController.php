<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service\Implement\BookmarkService;


class BookmarkController
{
    public $bookmarkService;

    public function __construct(BookmarkService $bookmarkService)
    {
        $this->bookmarkService = $bookmarkService;
    }

    public function getAll()
    {
        $bookmarks = $this->bookmarkService->getAll();
        return response()->json($bookmarks);
    }

    public function create(Request $request)
    {
        $this->bookmarkService->create($request);
        return response()->json("Created");
    }

    public function show(int $id)
    {
        $user = $this->bookmarkService->show($id);
        return response()->json($user);
    }

    public function update(Request $request, int $id)
    {
        $this->bookmarkService->update($request, $id);
        return response()->json("Updated");
    }

    public function delete(int $id)
    {
        $this->bookmarkService->delete($id);
        return response()->json("Deleted");
    }

    public function getByUserAndMap(Request $request)
    {
        $bookmarks = $this->bookmarkService->getByUserAndMap($request);
        return response()->json($bookmarks);
    }
}
