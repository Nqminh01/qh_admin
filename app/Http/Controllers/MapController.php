<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service\Implement\MapService;


class MapController
{
    public $mapService;

    public function __construct(MapService $mapService)
    {
        $this->mapService = $mapService;
    }

    public function getAll()
    {
        $maps = $this->mapService->getAll();
        return response()->json($maps);
    }

    public function show(int $id)
    {
        $map = $this->mapService->show($id);
        return response()->json($map);
    }

    public function create(Request $request)
    {
        $this->mapService->create($request);
        return response()->json("Created");
    }

    public function update(Request $request, int $id)
    {
        $this->mapService->update($request, $id);
        return response()->json("Updated");
    }

    public function delete(int $id)
    {
        $this->mapService->delete($id);
        return response()->json("Deleted");
    }

    public function findByWMS(Request $wms)
    {
        $map = $this->mapService->findByWMS($wms);
        return response()->json($map);
    }
}
