<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserMap extends Model
{
    use HasFactory;

    protected $table = 'users_map';

    protected $fillable = [
        'user_name',
        'map_id',
    ];
}
