<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{
    use HasFactory;

    protected $table = 'bookmark';

    protected $fillable = [
        'map_id',
        'user_name',
        'bookmark_name',
        'extent',
        'selected_layer'
    ];
}
