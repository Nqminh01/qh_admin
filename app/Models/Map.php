<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
    use HasFactory;

    protected $table = 'map';

    protected $fillable = [
        'map_name',
        'wms_url',
        'wfs_url',
        'description',
    ];
}
