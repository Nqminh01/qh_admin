<?php

namespace App\Service\Interface;

interface IMapService
{
    public function getAll();

    public function show(int $id);

    public function create(object $data): bool;

    public function update(object $user, int $id): bool;

    public function delete(int $id): bool;

    public function findByWMS(object $data);
}
