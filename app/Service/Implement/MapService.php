<?php

namespace App\Service\Implement;

use App\Models\Map;
use App\Service\Interface\IMapService;
use App\Repository\Implement\MapRepository;

class MapService implements IMapService
{
    public $mapRepository;

    public function __construct(MapRepository $mapRepository)
    {
        $this->mapRepository = $mapRepository;
    }

    public function getAll()
    {
        $maps = $this->mapRepository->getAll();
        return $maps;
    }

    public function show(int $id)
    {
        $map = $this->mapRepository->show($id);
        return $map;
    }

    public function create(object $data): bool
    {
        // $map = new User();
        // $map->user_name = $data->input('user_name');
        // $map->password = $data->input('password');

        // return $this->userRepository->create($user);
        return true;
    }

    public function update(object $data, int $id): bool
    {
        // $user = new User();
        // $user->password = $data->input('password');
        // return $this->userRepository->update($user, $id);
        return true;
    }

    public function delete(int $id): bool
    {
        // return $this->userRepository->delete($id);
        return true;
    }

    public function findByWMS(object $data)
    {
        $map = $this->mapRepository->findByWMS($data->wms);
        return $map;
    }
}
