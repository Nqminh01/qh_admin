<?php

namespace App\Service\Implement;

use App\Models\Bookmark;
use App\Service\Interface\IBookmarkService;
use App\Repository\Implement\BookmarkRepository;

class BookmarkService implements IBookmarkService
{
    public $bookmarkRepository;

    public function __construct(BookmarkRepository $bookmarkRepository)
    {
        $this->bookmarkRepository = $bookmarkRepository;
    }

    public function getAll()
    {
        $bookmarks = $this->bookmarkRepository->getAll();
        return $bookmarks;
    }

    public function show(int $id)
    {
        $bookmark = $this->bookmarkRepository->show($id);
        return $bookmark;
    }

    public function create(object $data): bool
    {
        $bookmark = new Bookmark();
        $bookmark->bookmark_name = $data->input('bookmark_name');
        $bookmark->user_name = $data->input('user_name');
        $bookmark->map_id = $data->input('map_id');
        $bookmark->extent = $data->input('extent');
        $bookmark->selected_layer = $data->input('selected_layer');

        return $this->bookmarkRepository->create($bookmark);
    }

    public function update(object $data, int $id): bool
    {
        // $bookmark = new Bookmark();
        // $bookmark->user_name = $data->input('user_name');
        // $bookmark->password = $data->input('password');
        // return $this->userRepository->update($user, $id);
        return true;
    }

    public function delete(int $id): bool
    {
        return $this->bookmarkRepository->delete($id);
    }

    public function getByUserAndMap(object $data)
    {
        $bookmarks = $this->bookmarkRepository->getByUserAndMap($data->user_name, $data->map_id);
        return $bookmarks;
    }
}
