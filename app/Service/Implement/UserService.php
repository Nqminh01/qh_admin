<?php

namespace App\Service\Implement;

use App\Models\User;
use App\Service\Interface\IUserService;
use App\Repository\Implement\UserRepository;

class UserService implements IUserService
{
    public $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getAll()
    {
        $users = $this->userRepository->getAll();
        return $users;
    }

    public function show(int $id)
    {
        $user = $this->userRepository->show($id);
        return $user;
    }

    public function create(object $data): bool
    {
        $user = new User();
        $user->user_name = $data->input('user_name');
        $user->password = $data->input('password');

        return $this->userRepository->create($user);
    }

    public function update(object $data, int $id): bool
    {
        $user = new User();
        $user->user_name = $data->input('user_name');
        $user->password = $data->input('password');
        return $this->userRepository->update($user, $id);
    }

    public function delete(int $id): bool
    {
        return $this->userRepository->delete($id);
    }
}
