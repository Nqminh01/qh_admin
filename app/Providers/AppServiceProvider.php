<?php

namespace App\Providers;

use App\Repository;
use App\Service;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }

    public $bindings = [
        //Repository
        Repository\Interface\IUserRepository::class => Repository\Implement\UserRepository::class,
        Repository\Interface\IBookmarkRepository::class => Repository\Implement\BookmarkRepository::class,
        Repository\Interface\IMapRepository::class => Repository\Implement\MapRepository::class,

        //Service
        Service\Interface\IUserService::class => Service\Implement\UserService::class,
        Service\Interface\IBookmarkService::class => Service\Implement\BookmarkService::class,
        Service\Interface\IMapService::class => Service\Implement\MapService::class,

    ];
}
