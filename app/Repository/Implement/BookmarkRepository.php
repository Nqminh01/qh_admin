<?php

namespace App\Repository\Implement;

use App\Models\Bookmark;
use App\Models\User;
use App\Repository\Interface\IBookmarkRepository;

class BookmarkRepository implements IBookmarkRepository
{

    public function getAll()
    {
        $bookmarks = Bookmark::all();

        return $bookmarks;
    }

    public function show(int $id): Bookmark
    {
        $bookmark = Bookmark::find($id);
        return $bookmark;
    }

    public function create(Bookmark $bookmark): bool
    {
        $newBookmark = $bookmark;
        $newBookmark->save();
        return true;
    }

    public function update(Bookmark $bookmark, int $id): bool
    {
        $updateBookmark = Bookmark::find($id);
        if ($updateBookmark) {
            $updateBookmark = $bookmark;
            $updateBookmark->save();
            return true;
        }
        return false;
    }

    public function delete(int $id): bool
    {
        $bookmark = User::find($id);
        if ($bookmark) {
            $bookmark->delete();
            return true;
        }
        return false;
    }

    public function search(string $keyword)
    {
    }

    public function getByUserAndMap(string $user_name, int $map_id)
    {
        $bookmarks = Bookmark::where('user_name', $user_name)->where('map_id', $map_id)->get();
        return $bookmarks;
    }
}
