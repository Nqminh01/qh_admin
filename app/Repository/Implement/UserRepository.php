<?php

namespace App\Repository\Implement;

use App\Models\User;
use App\Repository\Interface\IUserRepository;

class UserRepository implements IUserRepository
{

    public function getAll()
    {
        $users = User::all();

        return $users;
    }

    public function show(int $id): User
    {
        $users = User::find($id);
        return $users;
    }

    public function create(User $user): bool
    {
        $check = User::where('user_name', $user->user_name)->first();
        if (!$check) {
            $newUser = $user;
            $newUser->save();
            return true;
        }
        return false;
    }

    public function update(User $user, int $id): bool
    {
        $updateUser = User::find($id);
        if ($updateUser) {
            $updateUser->user_name = $user->user_name;
            $updateUser->password = $user->password;
            $updateUser->save();
            return true;
        }
        return false;
    }

    public function delete(int $id): bool
    {
        $users = User::find($id);
        if ($users) {
            $users->delete();
            return true;
        }
        return false;
    }

    public function search(string $keyword)
    {
    }
}
