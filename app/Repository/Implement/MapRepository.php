<?php

namespace App\Repository\Implement;

use App\Models\Map;
use App\Repository\Interface\IMapRepository;

class MapRepository implements IMapRepository
{

    public function getAll()
    {
        $maps = Map::all();

        return $maps;
    }

    public function show(int $id): Map
    {
        $map = Map::find($id);
        return $map;
    }

    public function create(Map $map): bool
    {
        $check = Map::where('map_name', $map->map_name)->first();
        if (!$check) {
            $newMap = $map;
            $newMap->save();
            return true;
        }
        return false;
    }

    public function update(Map $map, int $id): bool
    {
        $updateMap = Map::find($id);
        if ($updateMap) {
            $updateMap = $map;
            $updateMap->save();
            return true;
        }
        return false;
    }

    public function delete(int $id): bool
    {
        $map = Map::find($id);
        if ($map) {
            $map->delete();
            return true;
        }
        return false;
    }

    public function search(string $keyword)
    {
    }

    public function findByWMS(string $wms)
    {
        $map = Map::where("wms_url", $wms)->first();
        return $map;
    }
}
