<?php

namespace App\Repository\Interface;

use App\Models\Bookmark;

interface IBookmarkRepository
{
    public function getAll();

    public function show(int $id): Bookmark;

    public function create(Bookmark $bookmark): bool;

    public function update(Bookmark $bookmark, int $id): bool;

    public function delete(int $id): bool;

    public function search(string $keyword);

    public function getByUserAndMap(string $user_name, int $map_id);
}
