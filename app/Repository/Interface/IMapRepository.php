<?php

namespace App\Repository\Interface;

use App\Models\Map;

interface IMapRepository
{
    public function getAll();

    public function show(int $id): Map;

    public function create(Map $map): bool;

    public function update(Map $map, int $id): bool;

    public function delete(int $id): bool;

    public function search(string $keyword);

    public function findByWMS(string $wms);
}
