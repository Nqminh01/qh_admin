<?php

namespace App\Repository\Interface;

use App\Models\User;

interface IUserRepository
{
    public function getAll();

    public function show(int $id): User;

    public function create(User $user): bool;

    public function update(User $user, int $id): bool;

    public function delete(int $id): bool;

    public function search(string $keyword);
}
